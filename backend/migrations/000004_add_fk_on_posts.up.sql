-- Add a new serial column
ALTER TABLE
    posts
ADD
    COLUMN new_id serial PRIMARY KEY;

-- Copy data from the old id to the new_id column
UPDATE
    posts
SET
    new_id = id;

-- Drop the old id column
ALTER TABLE
    posts DROP COLUMN id;

-- Rename the new_id column to id
ALTER TABLE
    posts RENAME COLUMN new_id TO id;

alter table
    posts
add
    column belongs_to bigint references posts default null;