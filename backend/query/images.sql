-- name: GetImage :one
SELECT
    *
FROM
    images
WHERE
    id = $1
LIMIT
    1;

-- name: ListImages :many
SELECT
    *
FROM
    images
ORDER BY
    id;

-- name: CreateImage :one
INSERT INTO
    images (
        sha1,
        md5,
        audio,
        video,
        file_type,
        thumb_type,
        width,
        height,
        thumb_width,
        thumb_height,
        size,
        duration,
        title,
        artist
    )
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11,
        $12,
        $13,
        $14
    ) RETURNING *;

-- name: UpdateImage :one
UPDATE
    images
SET
    sha1 = $2,
    md5 = $3,
    audio = $4,
    video = $5,
    file_type = $6,
    thumb_type = $7,
    width = $8,
    height = $9,
    thumb_width = $10,
    thumb_height = $11,
    size = $12,
    duration = $13,
    title = $14,
    artist = $15
WHERE
    id = $1 RETURNING *;

-- name: DeleteImage :exec
DELETE FROM
    images
WHERE
    id = $1;