-- name: GetBan :one
SELECT
    *
FROM
    bans
WHERE
    id = $1
LIMIT
    1;

-- name: ListBans :many
SELECT
    *
FROM
    bans
ORDER BY
    id;

-- You can order by a suitable column in your "bans" table
-- name: CreateBan :one
INSERT INTO
    bans (
        ip_address,
        ban_reason,
        banned_by,
        expiration_date,
        is_permanent
    )
VALUES
    ($1, $2, $3, $4, $5) RETURNING *;

-- name: UpdateBan :one
UPDATE
    bans
SET
    ip_address = $2,
    ban_reason = $3,
    banned_by = $4,
    expiration_date = $5,
    is_permanent = $6
WHERE
    id = $1 RETURNING *;

-- name: DeleteBan :exec
DELETE FROM
    bans
WHERE
    id = $1;