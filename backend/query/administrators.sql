-- name: GetAdministrator :one
SELECT
    *
FROM
    administrators
WHERE
    username = $1
    and password = $2
LIMIT
    1;

-- name: ListAdministrators :many
SELECT
    *
FROM
    administrators
ORDER BY
    id;

-- You can order by a suitable column in your "administrators" table
-- name: CreateAdministrator :one
INSERT INTO
    administrators (
        username,
        password,
        email,
        full_name,
        created_at,
        last_login
    )
VALUES
    ($1, $2, $3, $4, $5, $6) RETURNING *;

-- name: UpdateAdministrator :one
UPDATE
    administrators
SET
    username = $2,
    password = $3,
    email = $4,
    full_name = $5,
    created_at = $6,
    last_login = $7
WHERE
    id = $1 RETURNING *;

-- name: DeleteAdministrator :exec
DELETE FROM
    administrators
WHERE
    id = $1;