-- name: GetPost :one
SELECT
    *
FROM
    posts
WHERE
    id = $1
LIMIT
    1;

-- name: GetThreads :many
select
    *
from
    posts
WHERE
    is_thread = true;

-- name: GetThread :many
SELECT
    *
FROM
    posts
WHERE
    is_thread = true
    and belongs_to = $1
ORDER BY
    created_on DESC;

-- name: CreatePost :one
INSERT INTO
    posts (
        created_on,
        is_thread,
        page,
        open,
        sage,
        name,
        trip,
        flag,
        body,
        ip_address,
        image,
        image_name,
        image_spoilered,
        belongs_to
    )
VALUES
    (
        NOW(),
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11,
        $12,
        $13
    ) RETURNING *;

-- name: UpdatePost :one
UPDATE
    posts
SET
    is_thread = $2,
    page = $3,
    open = $4,
    sage = $5,
    name = $6,
    trip = $7,
    flag = $8,
    body = $9,
    ip_address = $10,
    image = $11,
    image_name = $12,
    image_spoilered = $13
WHERE
    id = $1 RETURNING *;

-- name: DeletePost :exec
DELETE FROM
    posts
WHERE
    id = $1;