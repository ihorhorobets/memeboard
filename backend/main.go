package main

import (
	"database/sql"
	"memeboard/api"
	"memeboard/db"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {
	d, err := sql.Open("postgres", os.Getenv("POSTGRESQL_URL"))
	if err != nil {
		panic(err)
	}
	defer d.Close()

	queries := db.New(d)

	r := gin.Default()
	r.Use(InjectDB(queries))

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	v1 := r.Group("/v1/api")
	{
		v1.GET("/bans", api.GetBans)
		v1.POST("/bans", api.CreateBan)
		v1.DELETE("/bans/:id", api.DeleteBan)

		v1.POST("/posts", api.CreatePost)
		v1.DELETE("/posts/:id", api.DeletePost)

		v1.GET("/threads", api.GetThreads)
		v1.POST("/threads", api.CreateThread)
		v1.GET("/thread/:id", api.GetThread)
	}
	r.Run()
}

func InjectDB(queries *db.Queries) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("queries", queries)
		c.Next()
	}
}
