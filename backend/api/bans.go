package api

import (
	"database/sql"
	"memeboard/db"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sqlc-dev/pqtype"
)

type Ban struct {
	ID             int32  `json:"id"`
	IpAddress      string `json:"ip_address"`
	BanReason      string `json:"ban_reason"`
	BannedAt       string `json:"banned_at"`
	BannedBy       string `json:"banned_by"`
	ExpirationDate string `json:"expiration_date"`
	IsPermanent    bool   `json:"is_permanent"`
}

func NewBan(ban *db.Ban) *Ban {
	return &Ban{
		ID:             ban.ID,
		IpAddress:      ban.IpAddress.IPNet.IP.String(),
		BanReason:      ban.BanReason.String,
		BannedAt:       ban.BannedAt.Time.String(),
		BannedBy:       ban.BannedBy.String,
		ExpirationDate: ban.ExpirationDate.Time.String(),
		IsPermanent:    ban.IsPermanent,
	}
}

func GetBans(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)
	bans, err := queries.ListBans(c.Request.Context())
	httpBans := make([]*Ban, len(bans))
	for i, ban := range bans {
		httpBans[i] = NewBan(&ban)
	}
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"bans": httpBans,
	})
}

type CreateBanRequest struct {
	IpAddress      string `json:"ip_address" binding:"required"`
	BanReason      string `json:"ban_reason" binding:"required"`
	ExpirationDate string `json:"expiration_date" binding:"required"`
	IsPermanent    bool   `json:"is_permanent" binding:"required"`
	Username       string `json:"login" binding:"required"`
	Password       string `json:"password" binding:"required"`
}

func CreateBan(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)
	var request CreateBanRequest
	err := c.BindJSON(&request)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	params := db.GetAdministratorParams{
		Username: request.Username,
		Password: request.Password,
	}
	admin, err := queries.GetAdministrator(c.Request.Context(), params)
	if err != nil {
		c.JSON(401, gin.H{
			"message": err.Error(),
		})
		return
	}
	ipAddres := pqtype.Inet{}
	err = ipAddres.Scan(request.IpAddress)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	expirationDate, err := time.Parse(request.ExpirationDate, "2006-01-02 15:04:05")
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	ban, err := queries.CreateBan(c.Request.Context(), db.CreateBanParams{
		IpAddress:      ipAddres,
		BanReason:      sql.NullString{String: request.BanReason, Valid: true},
		BannedBy:       sql.NullString{String: admin.Username, Valid: true},
		ExpirationDate: sql.NullTime{Time: expirationDate, Valid: true},
		IsPermanent:    request.IsPermanent,
	})

	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, NewBan(&ban))

}

func DeleteBan(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)
	id := c.Param("id")
	idInt, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	err = queries.DeleteBan(c.Request.Context(), int32(idInt))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"message": "ban deleted",
	})
}
