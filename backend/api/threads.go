package api

import (
	"database/sql"
	"memeboard/db"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sqlc-dev/pqtype"
)

type Post struct {
	ID        int64     `json:"id"`
	CreatedOn time.Time `json:"created_on"`
	IsThread  bool      `json:"is_thread"`
	BelongsTo *int64    `json:"belongs_to"`
	Open      bool      `json:"open"`
	Sage      bool      `json:"sage"`
	Name      string    `json:"name"`
	Trip      string    `json:"trip"`
	Flag      string    `json:"flag"`
	Body      string    `json:"body"`
}

func NewPost(post *db.Post) *Post {
	return &Post{
		ID:        post.ID,
		CreatedOn: post.CreatedOn,
		IsThread:  post.IsThread,
		BelongsTo: &post.BelongsTo.Int64,
		Open:      post.Open,
		Sage:      post.Sage,
		Name:      post.Name.String,
		Trip:      post.Trip.String,
		Flag:      post.Flag.String,
		Body:      post.Body.String,
	}
}

func GetThreads(c *gin.Context) {

	queries := c.MustGet("queries").(*db.Queries)
	threads, err := queries.GetThreads(c.Request.Context())

	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	httpPosts := make([]*Post, len(threads))
	for _, thread := range threads {
		httpPosts = append(httpPosts, NewPost(&thread))
	}

	c.JSON(200, gin.H{
		"threads": httpPosts,
	})
}

func GetThread(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)

	id := c.Param("id")
	dbId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	posts, err := queries.GetThread(c.Request.Context(), sql.NullInt64{Int64: dbId, Valid: true})
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	httpPosts := make([]*Post, len(posts))
	for _, thread := range posts {
		httpPosts = append(httpPosts, NewPost(&thread))
	}

	c.JSON(200, gin.H{
		"posts": httpPosts,
	})
}

type CreateThreadRequest struct {
	Sage bool   `json:"sage"`
	Name string `json:"name"`
	Body string `json:"body"`
}

func CreateThread(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)
	var request CreateThreadRequest
	err := c.BindJSON(&request)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	ip := pqtype.Inet{}
	err = ip.Scan(c.ClientIP())
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	thread, err := queries.CreatePost(c.Request.Context(), db.CreatePostParams{
		IsThread:  true,
		Open:      true,
		Sage:      request.Sage,
		Name:      sql.NullString{String: request.Name, Valid: true},
		Body:      sql.NullString{String: request.Body, Valid: true},
		IpAddress: ip,
	})

	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"thread": NewPost(&thread),
	})
}

type DeletePostRequest struct {
	Username string `json:"login" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func DeletePost(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)

	id := c.Param("id")
	dbId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	var request DeletePostRequest
	err = c.BindJSON(&request)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	_, err = queries.GetAdministrator(c.Request.Context(), db.GetAdministratorParams{
		Username: request.Username,
		Password: request.Password,
	})
	if err != nil {
		c.JSON(401, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = queries.DeletePost(c.Request.Context(), dbId)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"message": "thread deleted",
	})
}

type CreatePostRequest struct {
	Sage     bool   `json:"sage"`
	Name     string `json:"name"`
	Body     string `json:"body"`
	ThreadID int64  `json:"thread_id"`
}

func CreatePost(c *gin.Context) {
	queries := c.MustGet("queries").(*db.Queries)
	var request CreatePostRequest
	err := c.BindJSON(&request)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	ip := pqtype.Inet{}
	err = ip.Scan(c.ClientIP())
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	thread, err := queries.CreatePost(c.Request.Context(), db.CreatePostParams{
		IsThread:  false,
		Open:      true,
		Sage:      request.Sage,
		Name:      sql.NullString{String: request.Name, Valid: true},
		Body:      sql.NullString{String: request.Body, Valid: true},
		IpAddress: ip,
		BelongsTo: sql.NullInt64{Int64: request.ThreadID, Valid: true},
	})

	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"thread": NewPost(&thread),
	})
}
