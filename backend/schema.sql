create domain uint15 as smallint check (value >= 0);

create table images (
    id bigserial primary key,
    sha1 bytea not null check (octet_length(sha1) = 20),
    md5 bytea not null check (octet_length(md5) = 16),
    audio bool not null,
    video bool not null,
    file_type text not null,
    thumb_type text not null,
    width uint15 not null,
    height uint15 not null,
    thumb_width uint15 not null,
    thumb_height uint15 not null,
    size bigint not null check (size > 0),
    duration int not null check (duration >= 0),
    title varchar(200),
    artist varchar(100)
);

create table posts (
    id bigserial primary key,
    created_on timestamptz not null default now(),
    is_thread BOOLEAN not null default false,
    belongs_to bigint references posts default null,
    page int not null default 0 check (page >= 0),
    open bool not null default true,
    sage bool not null default false,
    name varchar(100),
    trip varchar(100),
    flag char(2),
    body text,
    ip_address inet not null,
    image bigint references images,
    image_name varchar(200) not null default '',
    image_spoilered bool not null default false
);

CREATE TABLE bans (
    id SERIAL PRIMARY KEY,
    ip_address INET NOT NULL,
    ban_reason TEXT,
    banned_at TIMESTAMPTZ DEFAULT NOW(),
    banned_by VARCHAR(255),
    expiration_date TIMESTAMPTZ,
    is_permanent BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE administrators (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) UNIQUE NOT NULL,
    password TEXT NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    full_name VARCHAR(255),
    created_at TIMESTAMPTZ DEFAULT NOW(),
    last_login TIMESTAMPTZ
);